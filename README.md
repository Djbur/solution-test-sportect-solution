# Sports Analytics Platform

## Overview
The Sports Analytics Platform is a cloud-native solution that ingests real-time sports data, stores it in DynamoDB, processes the data to calculate statistics, and provides API endpoints for data retrieval.

## Prerequisites
- Node.js (version 14 or higher)
- AWS CLI configured with appropriate credentials

## Deployment
1. Install the AWS CLI.
2. Configure your AWS CLI credentials.
3. Install Node.js and npm.
4. Clone this repository.
5. Navigate to the project root directory.
6. Run `npm install` to install dependencies.
7. Run `npm run build` to compile TypeScript code.
8. Run `npm run deploy` to deploy the infrastructure using AWS CDK.

## API Endpoints
- `POST /ingest`: Ingest real-time sports data (JSON payload representing match events).
- `GET /matches`: Retrieve a list of all matches.
- `GET /matches/{match_id}`: Retrieve details of a specific match.
- `GET /matches/{match_id}/statistics`: Retrieve statistics for a specific match.
- `GET /teams/{team_name}/statistics`: Retrieve statistics for a specific team across all matches.

## Assumptions and Design Decisions
- The ingestion API accepts JSON payloads representing match events.
- The DynamoDB table schema efficiently stores and queries the ingested data.
- The processing Lambda function calculates statistics and stores them in a separate DynamoDB table.
- Basic monitoring using AWS CloudWatch is implemented for the processing Lambda function.
- Unit tests for Lambda functions are provided but may need further enhancements for comprehensive coverage.

Statistics Retrieval
You can obtain statistics related to a specific match through GET requests to the following endpoints:

Get match details: GET http://api.example.com/matches/{match_id}
Get match statistics: GET http://api.example.com/matches/{match_id}/statistics
Contribution
If you want to contribute to this project, follow these steps:

Fork the repository.
Create a branch for your new feature or fix: git checkout -b feature-new-feature.
Make your changes and commit: git commit -m "Add a new feature".
Push to your branch: git push origin feature-new-feature.
Open a pull request on GitHub.
Known Issues
List any known issues that users may encounter.
Contact
If you have any questions or issues, feel free to contact us at support@example.com.

License
This project is licensed under the MIT License. See the LICENSE file for more details.


