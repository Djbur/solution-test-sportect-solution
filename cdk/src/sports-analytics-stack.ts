import * as cdk from 'aws-cdk-lib';
import * as dynamodb from 'aws-cdk-lib/aws-dynamodb';
import * as lambda from 'aws-cdk-lib/aws-lambda';
import * as apigateway from 'aws-cdk-lib/aws-apigateway';
import * as cloudwatch from 'aws-cdk-lib/aws-cloudwatch';
import * as cloudwatchactions from 'aws-cdk-lib/aws-cloudwatch-actions';
import * as subscriptions from 'aws-cdk-lib/aws-sns-subscriptions';
import * as sns from 'aws-cdk-lib/aws-sns';
import { StartingPosition } from 'aws-cdk-lib/aws-lambda-event-sources';
import { Metric, MetricProps } from 'aws-cdk-lib/aws-cloudwatch';

export class SportsAnalyticsStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // DynamoDB Table for events
    const matchEventsTable = new dynamodb.Table(this, 'MatchEventsTable', {
      partitionKey: { name: 'match_id', type: dynamodb.AttributeType.STRING },
      billingMode: dynamodb.BillingMode.PAY_PER_REQUEST,
      removalPolicy: cdk.RemovalPolicy.DESTROY,
      stream: dynamodb.StreamViewType.NEW_IMAGE, // Habilitar el flujo de eventos
    });

    // DynamoDB Table for statics
    const matchStatisticsTable = new dynamodb.Table(this, 'MatchStatisticsTable', {
      partitionKey: { name: 'match_id', type: dynamodb.AttributeType.STRING },
      billingMode: dynamodb.BillingMode.PAY_PER_REQUEST,
      removalPolicy: cdk.RemovalPolicy.DESTROY, // Solo para fines de prueba, cambiar según los requisitos
    });

    // Ingestion Lambda
    const ingestionLambda = new lambda.Function(this, 'IngestionLambda', {
      runtime: lambda.Runtime.NODEJS_14_X,
      handler: 'index.handler',
      code: lambda.Code.fromAsset('path/to/ingestion-lambda'),
      environment: {
        TABLE_NAME: matchEventsTable.tableName,
      },
    });

    // Lambda Processing
    const processingLambda = new lambda.Function(this, 'ProcessingLambda', {
      runtime: lambda.Runtime.NODEJS_14_X,
      handler: 'index.handler',
      code: lambda.Code.fromAsset('path/to/processing-lambda'),
      environment: {
        TABLE_NAME: matchStatisticsTable.tableName,
      },
    });

    // Assign Permissions for the DynamoDB Stream to the Processing Lambda
    processingLambda.addEventSourceMapping('ProcessingLambdaEventSource', {
      eventSourceArn: matchEventsTable.tableStreamArn!,
      batchSize: 1,
      startingPosition: StartingPosition.TRIM_HORIZON,
    });

    // API Gateway
    const api = new apigateway.RestApi(this, 'SportsAnalyticsAPI');

    // Endpoint de income
    const ingestEndpoint = api.root.addResource('ingest');
    const ingestionIntegration = new apigateway.LambdaIntegration(ingestionLambda);
    ingestEndpoint.addMethod('POST', ingestionIntegration);

    // Data recovery endpoint
    const matchesEndpoint = api.root.addResource('matches');
    matchesEndpoint.addMethod('GET', new apigateway.MockIntegration());

    const matchEndpoint = matchesEndpoint.addResource('{match_id}');
    matchEndpoint.addMethod('GET', new apigateway.MockIntegration());

    const matchStatisticsEndpoint = matchEndpoint.addResource('statistics');
    matchStatisticsEndpoint.addMethod('GET', new apigateway.MockIntegration());

    const teamsEndpoint = api.root.addResource('teams');
    const teamStatisticsEndpoint = teamsEndpoint.addResource('{team_name}/statistics');
    teamStatisticsEndpoint.addMethod('GET', new apigateway.MockIntegration());

    // CloudWatch Alarm for statics
    const statisticsAlarm = new cloudwatch.Alarm(this, 'StatisticsAlarm', {
      metric: new Metric({
        namespace: 'AWS/DynamoDB',
        metricName: 'ConsumedReadCapacityUnits',
        dimensionsMap: {
          TableName: matchStatisticsTable.tableName,
        },
        statistic: 'Sum',
      } as MetricProps),
      threshold: 1000, // Threshold to generate an alarm (adjust according to requirements)
      evaluationPeriods: 1,
      alarmName: 'MatchStatisticsAlarm',
    });

    // Topic SNS to alarms
    const snsTopic = new sns.Topic(this, 'SportsAnalyticsTopic');
    snsTopic.addSubscription(new subscriptions.EmailSubscription('your-email@example.com'));

    // Associate the alarm action with sending email notifications
    statisticsAlarm.addAlarmAction(new cloudwatchactions.SnsAction(snsTopic));
  }
}