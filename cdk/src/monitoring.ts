import * as cdk from 'aws-cdk-lib';
import * as cloudwatch from 'aws-cdk-lib/aws-cloudwatch';
import * as cloudwatchactions from 'aws-cdk-lib/aws-cloudwatch-actions';
import * as subscriptions from 'aws-cdk-lib/aws-sns-subscriptions';
import * as sns from 'aws-cdk-lib/aws-sns';


// Function to add a CloudWatch alarm for a specific resource

export function addCloudWatchAlarmForResource(
  scope: cdk.Construct,
  resourceName: string,
  metricName: string,
  threshold: number,
  statistic: cloudwatch.Statistic,
  evaluationPeriods: number,
  alarmName: string,
  topic: sns.Topic
) {
  const metric = new cloudwatch.Metric({
    namespace: 'AWS/DynamoDB',
    metricName,
    dimensionsMap: {
      TableName: resourceName,
    },
    statistic,
  });

  const alarm = new cloudwatch.Alarm(scope, alarmName, {
    metric,
    threshold,
    evaluationPeriods,
  });

  alarm.addAlarmAction(new cloudwatchactions.SnsAction(topic));
}