import * as cdk from 'aws-cdk-lib';
import { SportsAnalyticsStack } from './sports-analytics-stack';

const app = new cdk.App();
new SportsAnalyticsStack(app, 'SportsAnalyticsStack');