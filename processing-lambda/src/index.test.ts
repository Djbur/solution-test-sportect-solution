import { handler } from './index';
import { DynamoDBStreamEvent } from 'aws-lambda';
import { describe, test, expect } from '@jest/globals';

describe('Processing Lambda', () => {
  test('should process the DynamoDB stream event', async () => {
    const event: DynamoDBStreamEvent = {
      Records: [
        {
          eventID: 'abc123',
          eventName: 'INSERT',
          eventVersion: '1.1',
          eventSource: 'aws:dynamodb',
          awsRegion: 'us-east-1',
          dynamodb: {
            Keys: {
              match_id: { S: '12345' },
            },
            NewImage: {
              match_id: { S: '12345' },
              timestamp: { S: '2023-06-22T19:45:30Z' },
              team: { S: 'FC Barcelona' },
              opponent: { S: 'Real Madrid' },
              event_type: { S: 'goal' },
              event_details: {
                M: {
                  player: {
                    M: {
                      name: { S: 'Lionel Messi' },
                      position: { S: 'Forward' },
                      number: { N: '10' },
                    },
                  },
                  goal_type: { S: 'penalty' },
                  minute: { N: '30' },
                  assist: {
                    M: {
                      name: { S: 'Sergio Busquets' },
                      position: { S: 'Midfielder' },
                      number: { N: '5' },
                    },
                  },
                  video_url: { S: 'https://example.com/goal_video.mp4' },
                },
              },
            },
            SequenceNumber: '1234567890',
            SizeBytes: 123,
            ApproximateCreationDateTime: 1234567890,
            StreamViewType: 'NEW_IMAGE',
            eventSourceARN: 'event-source-arn',
          },
          eventSourceARN: 'event-source-arn',
        },
      ],
    } as any;

    const consoleSpy = jest.spyOn(console, 'log').mockImplementation();

    await handler(event);

    expect(consoleSpy).toHaveBeenCalledWith('Data processed successfully');

    consoleSpy.mockRestore();
  });
});