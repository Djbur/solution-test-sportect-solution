import { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';

export async function handler(event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> {
  try {
    const requestBody = JSON.parse(event.body!);

   // Perform data storage logic in DynamoDB

    const response = {
      status: 'success',
      message: 'Data successfully ingested.',
      data: {
        event_id: 'abc123',
        timestamp: requestBody.timestamp,
      },
    };

    return {
      statusCode: 200,
      body: JSON.stringify(response),
    };
  } catch (error) {
    return {
      statusCode: 400,
      body: JSON.stringify({
        status: 'error',
        message: 'Failed to ingest data.',
        error: error.message,
      }),
    };
  }
}