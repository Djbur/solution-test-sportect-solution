import { handler } from './index';
import { APIGatewayProxyEvent } from 'aws-lambda';
import { describe, test, expect } from '@jest/globals';

describe('Ingestion Lambda', () => {
  test('should return 200 with success message when data ingestion is successful', async () => {
    const event: APIGatewayProxyEvent = {
      body: JSON.stringify({
        match_id: '12345',
        timestamp: '2023-06-22T19:45:30Z',
        team: 'FC Barcelona',
        opponent: 'Real Madrid',
        event_type: 'goal',
        event_details: {
          player: {
            name: 'Lionel Messi',
            position: 'Forward',
            number: 10,
          },
          goal_type: 'penalty',
          minute: 30,
          assist: {
            name: 'Sergio Busquets',
            position: 'Midfielder',
            number: 5,
          },
          video_url: 'https://example.com/goal_video.mp4',
        },
      }),
    } as any;

    const result = await handler(event);
    const body = JSON.parse(result.body!);

    expect(result.statusCode).toBe(200);
    expect(body.status).toBe('success');
    expect(body.message).toBe('Data successfully ingested.');
    expect(body.data.event_id).toBeDefined();
    expect(body.data.timestamp).toBe('2023-06-22T19:45:30Z');
  });

  test('should return 400 with error message when required fields are missing', async () => {
    const event: APIGatewayProxyEvent = {
      body: JSON.stringify({
        timestamp: '2023-06-22T19:45:30Z',
        team: 'FC Barcelona',
        opponent: 'Real Madrid',
        event_type: 'goal',
        event_details: {
          player: {
            name: 'Lionel Messi',
            position: 'Forward',
            number: 10,
          },
          goal_type: 'penalty',
          minute: 30,
          assist: {
            name: 'Sergio Busquets',
            position: 'Midfielder',
            number: 5,
          },
          video_url: 'https://example.com/goal_video.mp4',
        },
      }),
    } as any;

    const result = await handler(event);
    const body = JSON.parse(result.body!);

    expect(result.statusCode).toBe(400);
    expect(body.status).toBe('error');
    expect(body.message).toBe('Failed to ingest data.');
    expect(body.error).toBe('Missing required fields');
  });
});